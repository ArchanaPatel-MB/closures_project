
function counterFactory() {
  var Counter = 0;
  function changeBy(val) {
    return Counter += val;
  }

  return {
    increment: function () {
      return changeBy(1);
    },

    decrement: function () {
      return changeBy(-1);
    }
  };
}



module.exports = counterFactory;